-- gui manager

module('GUIManager', package.seeall)

function GUIManager.new()
  local self = {}
  setmetatable(self, {__index = GUIManager})
  return self
end

function GUIManager.createPartition(layer)
  self.partition = MOAIPartition.new()
  layer:setPartition(self.partition)
end