-- couriers project

-- copyright Matthew Pace
-- www.matthewdpace.com

-- main.lua

-- requires
require 'state_manager'
require 'resource_manager'
require 'physics_manager'
require 'gui_manager'
require 'input_manager'
require 'utility'
require 'ship'

-- globals: game variabls
USE_KEYBOARD =  true

gameLayer = MOAILayer2D.new()
guiLayer = MOAILayer2D.new()
backgroundLayer = MOAILayer2D.new()

viewport = MOAIViewport.new()

camera = MOAICamera2D.new()

guiLayer:setViewport(viewport)
gameLayer:setViewport(viewport)
backgroundLayer:setViewport(viewport)

guiLayer:setCamera(camera)
gameLayer:setCamera(camera)
backgroundLayer:setCamera(camera)


-- Find screen size or use default
if (MOAIEnvironment.screenHeight == nil) or (MOAIEnvironment.screenWidth == nil) then
  screenX = 1280
  screenY = 800
else
  screenX = MOAIEnvironment.screenWidth
  screenY = MOAIEnvironment.screenHeight
end

MOAISim.openWindow('Couriers', screenX, screenY)
viewport:setSize(screenX, screenY)
viewport:setScale(screenX, screenY)

-- globals: game modules / objects
RM = ResourceManager.new()
PM = PhysicsManager.new()
SM = StateManager.new()
IM = InputManager.new()
GM = GUIManager.new()




PM.world:start()

renderTable = {backgroundLayer, gameLayer, guiLayer}
MOAIRenderMgr.setRenderTable(renderTable)
