-- physics_manager

module('PhysicsManager', package.seeall)

function PhysicsManager.new()
  local self = {}
  setmetatable(self, {__index = PhysicsManager})
  return self
end

function PhysicsManager:createDevWorld()
  self.world = MOAIBox2DWorld.new()
  self.world:setUnitsToMeters(.01)
  self.world:setGravity(0,-10)
end
