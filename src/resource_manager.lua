-- resource_manager.lua

module('ResourceManager', package.seeall)

local resourceTable = require 'resources'

local loader = {
  [1] = function (rsrc)       -- [1] MOAIGfxQuad2D
    local quad = MOAIGfxQuad2D.new()
    quad:setTexture(rsrc.assetLoc)
    quad:setRect(unpack(rsrc.rect))
    return quad
  end,
  
  [2] = function (rsrc)       -- [2] MOAIFont
    local font = MOAIFont.new()
    font:loadFromTTF(rsrc.assetLoc, rsrc.glyphs, rsrc.size, rsrc.dpi)
    return font
  end,
  
}

function ResourceManager.new()
  local self = {}
  self.cache = {}
  
  setmetatable(self, {__index = ResourceManager})
  return self
end

function ResourceManager:loadLevel(levelName)
  -- Loads everything in a level
  for _, resourceItem in pairs(resourceTable) do
    for _, level in pairs(resourceItem.levels) do
      if level == levelName then
        self:loadItem(resourceItem.ID)
      end
    end
  end
end

function ResourceManager:unloadLevel(levelName)
  -- removes everything related to level in self.cache
  for key,cacheItem in pairs(self.cache) do
    for _, level in pairs(cacheItem.levels) do
      if level == levelName then
        self.cache[key] = nil
      end
    end
  end
end

function ResourceManager:loadItem(itemID)
  -- handles the self.cache, defers to loader table for actual asset loading
  local tempItem = {}
  local rtItem = resourceTable[itemID]
  
  tempItem.levels = rtItem[levels]
  tempItem.object = loader[rtItem.assetType](rtItem)
  self.cache[itemID] = tempItem
end

function ResourceManager:unloadItem(itemID)
  self.cache[key] = nil
end


function ResourceManager:getItem(itemID)
  if (self.cache ~= nil) or (next(self.cache) == nil) then  
    for k,v in pairs(self.cache) do
      if k == itemID then
        return self.cache[itemID].object
      end
    end
  else
    self:loadItem(itemID)
    return self.cache[itemID].object
  end
end
