

local assets = '../assets/'

-- [1] MOAIGfxQuad2D
-- [2] MOAIFont


local meta = {
  __index = function (resourcesTable, key)
      for _,t in ipairs(resourcesTable) do
        if t.ID == key then
          return t
        end
      end
    end
  }

local resources = {
  {
    ID = 'buttonUp',
    assetType = 1,
    assetLoc = assets .. 'button_up_01.png',
    levels = {'GUI'},
    rect = {-80,-80,80,80}

  },
  {
    ID = 'buttonDown',
    assetType = 1,
    assetLoc = assets .. 'button_down_01.png',
    levels = {'GUI'},
    rect = {-80,-80,80,80}

  },
  {
    ID = 'devbackground',
    assetType = 1,
    assetLoc = assets .. 'devbackground.png',
    levels = {'dev'},
    rect = {0,0,5068,1204},
  }
}


setmetatable(resources, meta)
return resources
