-- state_manager.lua


module("StateManager", package.seeall)

require 'levels'

local states = {}

states['startup'] = {

  loadState = function ()
    print "entering loadState startup"
  end,

  unloadState = function ()
    print "you never need to unload the startup state, FYI"
  end
}

states['dev'] = {

  loadState = function ()
    -- load dev background into texture, into layer, into rendermgr
    RM:loadLevel('dev')
    RM:loadLevel('GUI')
    PM:createDevWorld()
  end,

  unloadState = function ()

  end,
}

function StateManager.new()
  local self = {}
  setmetatable(self, {__index = StateManager})
  self.activeStates = {}
  states['startup'].loadState()
  return self
end

function StateManager:startState(stateName)
  states[stateName].loadState()
  table.insert(self.activeStates, stateName)
end

function StateManager:endState(name)
  states[name].unloadState()
  for key, value in self.activeStates do
    if value == name then
      table.remove(self.activeStates, key)
      break
     end
   end
 end

